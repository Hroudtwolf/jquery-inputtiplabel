/**
 * A jQuery plugin for creating embedded label tips inside of a related
 * input element.
 * 
 * @author      Marc Sven Kleinböhl (aka Hroudtwolf); coding4food[AT]hroudtwolf[DOT]de
 * @copyright   2013+ © By Marc Sven Kleinboehl
 *  * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
$.fn.inputTipLabel = function(settings) {
  
  // Some early exceptions.
  if (typeof settings == 'undefined') {
    throw 'Need settings argument to proceed.';
  }
  
  if (typeof settings != 'object') {
    throw 'Settings must be an object.';
  }
  
  if (typeof settings.labelSelector == 'undefined') {
    throw 'Not labelSelector property found.';
  }    
  
  var labelElement = $(settings.labelSelector);
  var labelText    = labelElement.html();
  var thisElement  = $(this);
  
  // Label not need anymore.
  labelElement.hide();

  if (thisElement.attr('value') == '') {
 
    thisElement.addClass(settings.tipClass);
    thisElement.attr('value', labelText);
  }
  
  // Eventhanding for focus-in events on the related input field.
  thisElement.live('focusin', function() {
  
    if (! thisElement.hasClass(settings.tipClass)) {
      return true;
    }
 
    thisElement.removeClass(settings.tipClass);
    thisElement.attr('value', '');
    
    return true;
  });
  
  // Eventhanding for focus-out events on the related input field.
  thisElement.live('focusout', function() {
  
    if (thisElement.attr('value') != '') {
   
      return;
    }
  
    thisElement.addClass(settings.tipClass);
    thisElement.attr('value', labelText);
    
    return true;
  });
  
  // Eventhanding for subnmit events on the related form.
  thisElement.closest('form').submit(function() {
    if (! thisElement.hasClass(settings.tipClass)) {
      return true;
    }
    
    thisElement.removeClass(settings.tipClass);
    thisElement.attr('value', '');
    
    return true;
  });
    
  return $(this);
};
